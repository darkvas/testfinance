module.exports = {
  "env": {
    "es6": true,
    "node": true
  },
  "extends": "eslint:recommended",
  "rules": {
    "no-multiple-empty-lines": ["error", { "max": 1, "maxEOF": 1 }],
    "quotes": [
      "error",
      "single"
    ],
    "semi": [
      "error",
      "always"
    ],
    "indent": ["warn", 2, { "SwitchCase": 1 }]
  }
};
