'use strict';

const app = require('./src/app');
const http = require('http');
const winston = require('winston');
const config = require('./src/config/index');
const database = require('./src/database');

const port = normalizePort(config.port || 3000);
app.set('port', port);

const server = http.createServer(app);

server.on('error', onError);
server.on('listening', onListening);

database.connect(config)
  .then(() => server.listen(port))
  .catch(onError);

function normalizePort(val) {
  const port = parseInt(val, 10);

  if (isNaN(port)) {
    return val;
  }

  if (port >= 0) {
    return port;
  }

  return false;
}

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  const bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  switch (error.code) {
    case 'EACCES':
      winston.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      winston.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

function onListening() {
  const addr = server.address();
  const bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  winston.info('Listening on ' + bind);
}
