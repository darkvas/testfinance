const bodyParser = require('body-parser');
const express = require('express');
const path = require('path');
const winston = require('winston');
const router = require('./routes/index');

const app = express();

app.use(bodyParser.json({ strict: false, inflate: false, type: 'application/json' }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/public', express.static(path.join(__dirname, './public')));

app.use(router);

app.use(function (req, res, next) {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// eslint-disable-next-line no-unused-vars
app.use(function (err, req, res, next) {
  res.error = app.get('env') === 'dev' ? err : {};
  winston.error(err);
  res.status(err.status || 500).send({ message: err.message });
});

module.exports = app;
