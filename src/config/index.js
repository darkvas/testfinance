'use strict';

const convict = require('convict');

const conf = convict({
  env: {
    doc: 'The application environment.',
    format: ['prod', 'dev', 'test', 'stage'],
    default: 'dev',
    env: 'NODE_ENV',
    arg: 'env',
  },
  port: {
    doc: 'Port of the server.',
    format: 'port',
    default: 4000,
    env: 'PORT',
    arg: 'port',
  },
  mongo: {
    host: {
      doc: 'The host of the MongoDB.',
      format: String,
      default: 'localhost',
      env: 'MONGO_HOST',
      arg: 'mongoHost',
    },
    port: {
      doc: 'Port of the MongoDB.',
      format: 'port',
      default: 27017,
      env: 'MONGO_PORT',
      arg: 'mongoPort',
    },
    dbName: {
      doc: 'Name of the database in MongoDB.',
      format: String,
      default: 'db',
      env: 'MONGO_DB_NAME',
      arg: 'mongoDbName',
    },
    user: {
      doc: 'Username used in the admin database.',
      format: String,
      default: '',
      env: 'DB_ADMIN_USER',
      arg: 'dbAdminUser',
    },
    password: {
      doc: 'Password used in the admin database.',
      format: String,
      default: '',
      env: 'DB_ADMIN_PASSWORD',
      arg: 'dbAdminPassword',
    },
  },
  winston: {
    consoleLogging: {
      doc: 'Enable logging to the console.',
      format: Boolean,
      default: true,
      env: 'WINSTON_CONSOLE_LOGGING',
      arg: 'winstonConsoleLogging',
    },
    colorize: {
      doc: 'Enable colored logging.',
      format: Boolean,
      default: false,
      env: 'WINSTON_COLORIZE',
      arg: 'winstonColorize',
    },
  },
});

conf.validate({allowed: 'strict'});

module.exports = conf.getProperties();
