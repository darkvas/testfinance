module.exports = {
  USER_ROLE: {
    ADMIN: 'ADMIN',
    CLIENT: 'CLIENT',
  },

  MODELS: {
    USER: 'user',
    SESSION: 'session',
  },

  CURRENCY: {
    USD: 'usd',
    UAH: 'uah',
  },
};
