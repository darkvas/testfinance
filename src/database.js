'use strict';

const mongoose = require('mongoose');
const winston = require('winston');

const getMongoUrl = (config) => {
  let connectionString = 'mongodb://';
  if (config.mongo.user && config.mongo.password) {
    connectionString += `${config.mongo.user}:${config.mongo.password}@`;
  }
  return connectionString.concat(`${config.mongo.host}:${config.mongo.port}/${config.mongo.dbName}`);
};

module.exports.connect = function (config) {
  return new Promise((resolve, reject) => {
    const connectionString = getMongoUrl(config);
    mongoose.Promise = global.Promise;

    mongoose.connect(connectionString, err => {
      if (err) {
        winston.error('MongoDB connection error: ', err);
        return reject(err);
      }
      resolve();
    });

    mongoose.connection.on('error', function (error) {
      winston.error('MongoDB throw an exception: ', error);
      throw error;
    });
  });
};

module.exports.disconnect = function () {
  return new Promise((resolve, reject) => {
    mongoose.disconnect(err => {
      if (err) {
        winston.error('MongoDB disconnect error: ' + err);
        return reject(err);
      }
      resolve();
    });
  });
};
