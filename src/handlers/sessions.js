const CONST = require('../constants');
const RESPONSE = require('../constants/response');

const Session = function Session() {
  this.register = (req, res, userId, userType) => {
    /* eslint-disable no-param-reassign */
    req.session.loggedIn = true;
    req.session.uId = userId;
    req.session.type = userType;
    /* eslint-enable no-param-reassign */
    res.status(200).send({ success: RESPONSE.AUTH.LOG_IN });
  };

  this.kill = (req, res) => {
    if (req.session) {
      req.session.destroy();
    }

    res.status(200).send({ success: RESPONSE.AUTH.LOG_OUT });
  };

  this.isAuthenticatedUser = (req, res, next) => {
    if (req.session && req.session.uId && req.session.loggedIn) {
      return next();
    }

    const err = new Error(RESPONSE.AUTH.UN_AUTHORIZED);
    err.status = 401;
    return next(err);
  };

  this.isAdmin = (req, res, next) => {
    if (req.session && req.session.type === CONST.USER_ROLE.ADMIN) {
      return next();
    }

    const err = new Error(RESPONSE.AUTH.NO_PERMISSIONS);
    err.status = 403;
    return next(err);
  };
};

module.exports = Session;
