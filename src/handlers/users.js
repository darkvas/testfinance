'use strict';

const co = require('co');
const crypto = require('crypto');
const CONST = require('../constants');
const RESPONSE = require('../constants/response');
const User = require('../models/user');

const create = (req, res, next) => {
  co(function *() {
    const email = req.body.email;
    let pass = req.body.pass;

    if (!email || !pass) {
      const err = new Error(RESPONSE.NOT_ENOUGH_PARAMS);
      err.status = 400;
      return next(err);
    }

    const shaSum = crypto.createHash('sha256');
    shaSum.update(pass);
    pass = shaSum.digest('hex');

    const userData = {
      email,
      pass,
      userType: CONST.USER_ROLE.CLIENT,
    };

    const userWithSameEmail = yield User
      .findOne({ email: userData.email })
      .exec();

    if (userWithSameEmail) {
      return res.status(400).send({ error: 'Email is used' });
    }

    const user = new User(userData);
    yield user.save();

    return res.status(201).send(user);
  }).catch(next);
};

const getAll = (req, res, next) => {
  co(function *() {
    const sortField = req.query.orderBy || 'createdAt';
    const sortDirection = +req.query.order || 1;
    const sortOrder = {};
    const skipCount = ((req.query.page - 1) * req.query.count) || 0;
    const limitCount = req.query.count || 20;

    sortOrder[sortField] = sortDirection;

    const users = yield User.find({})
    // .select('email userType devices profile accounts')
      .sort(sortOrder)
      .skip(skipCount)
      .limit(limitCount)
      .exec();

    return res.status(200).send(users);
  }).catch(next);
};

const getCount = (req, res, next) => {
  co(function *() {
    const count = yield User.count();
    return res.status(200).send({ count });
  }).catch(next);
};

const updateById = (req, res, next) => {
  co(function *() {
    const userId = req.params.id;

    // TODO Validate input data
    const userData = req.body;
    const updated = yield User.update({ _id: userId }, { $set: userData });
    return res.status(200).send(updated);
  }).catch(next);
};

const getById = (req, res, next) => {
  co(function *() {
    const userId = req.params.id;

    const user = yield User
      .findOne({ _id: userId })
      // .select( 'login userType devices profile favorites accounts')
      .exec();

    if (!user) {
      return res.status(404).send({ error: `${RESPONSE.ON_ACTION.NOT_FOUND} with such _id: ${userId}` });
    }

    return res.status(200).send(user);
  }).catch(next);
};

const deleteById = (req, res, next) => {
  co(function *() {
    const userId = req.params.id;

    yield User.remove({ _id: userId });
    return res.status(200).send({ success: RESPONSE.ON_ACTION.SUCCESS });
  }).catch(next);
};

module.exports = {
  create,
  getAll,
  getById,
  getCount,
  updateById,
  deleteById
};
