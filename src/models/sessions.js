const CONST = require('../constants');
const mongoose = require('mongoose');

const sessionSchema = new mongoose.Schema({
  sessionId: {
    type: Boolean,
    required: true
  }
}, {
  strict: true,
});

module.exports = mongoose.model(CONST.MODELS.SESSION, sessionSchema);
