const CONST = require('../constants');
const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
    trim: true,
    lowercase: true,
    index: true,
    unique: true
  },
  pass: {
    type: String,
    required: true
  },
  role: {
    type: String,
    enum: [CONST.USER_ROLE.ADMIN, CONST.USER_ROLE.CLIENT],
    default: CONST.USER_ROLE.CLIENT,
    uppercase: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
    index: true
  },
  confirmToken: {
    type: String
  },
  profile: {
    firstName: { type: String },
    lastName: { type: String },
    birthDay: { type: String },
  },
}, {
  strict: true,
});

module.exports = mongoose.model(CONST.MODELS.USER, userSchema);
