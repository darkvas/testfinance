'use strict';

const router = require('express').Router();
const usersRouter = require('./users');

router.get('/ping', (req, res) => {
  res.status(200).send(`Pong: ${new Date()}`);
});

router.use('/user', usersRouter);

module.exports = router;
