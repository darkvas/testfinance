'use strict';

const router = require('express').Router();
const userHandler = require('../handlers/users');

router
  .route('/')
  .post(userHandler.create)
  .get(userHandler.getAll);

router
  .route('/count')
  .get(userHandler.getCount);

router
  .route('/:id')
  .put(userHandler.updateById)
  .get(userHandler.getById)
  .delete(userHandler.deleteById);

module.exports = router;
