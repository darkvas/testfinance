'use strict';

const config = require('../src/config/index');
const co = require('co');
const db = require('../src/database');

before(co.wrap(function * before() {
  yield db.connect({ mongo: { host: config.mongo.host, port: config.mongo.port, dbName: 'test' } });
}));

after(co.wrap(function * before() {
  yield db.disconnect();
}));
