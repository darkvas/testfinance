'use strict';

const request = require('supertest');
const expect = require('chai').expect;
const mongoose = require('mongoose');
const app = require('../../src/app');

describe('Manage Users API tests', () => {
  const agent = request.agent(app);
  let userOneId;

  it('should return 201 and create user 1', (done) => {
    const userData = require('../fixtures/userClient1.json');
    const data = {
      email: userData.email,
      pass: userData.pass,
    };

    agent
      .post('/user/')
      .send(data)
      .expect(201)
      .end((err, res) => {
        expect(res.body).to.have.property('_id');

        done(err);
      });
  });

  it('should return 201 and create user 2', (done) => {
    const userData = require('../fixtures/userClient2.json');
    const data = {
      email: userData.email,
      pass: userData.pass,
    };

    agent
      .post('/user/')
      .send(data)
      .expect(201)
      .end((err, res) => {
        expect(res.body).to.have.property('_id');

        done(err);
      });
  });

  it('should return 400 cause email is used', (done) => {
    const userData = require('../fixtures/userClient2.json');
    const data = {
      email: userData.email,
      pass: userData.pass,
    };

    agent
      .post('/user/')
      .send(data)
      .expect(400)
      .end((err, res) => {
        expect(res.body).to.have.property('error');

        done(err);
      });
  });

  it('should return 200 and list of users', (done) => {
    const user1 = require('../fixtures/userClient1.json');
    const user2 = require('../fixtures/userClient2.json');

    agent
      .get('/user/')
      .expect(200)
      .end((err, res) => {
        expect(res.body).to.be.instanceof(Array);
        expect(res.body).to.be.lengthOf(2);

        expect(res.body[0]).to.have.property('email', user1.email);
        expect(res.body[1]).to.have.property('email', user2.email);
        expect(res.body[0]).to.have.property('_id');
        userOneId = res.body[0]._id;

        done(err);
      });
  });

  it('should return 200 and users count', (done) => {
    agent
      .get('/user/count')
      .expect(200)
      .end((err, res) => {
        expect(res.body).to.have.property('count');
        expect(res.body.count).equal(2);

        done(err);
      });
  });

  it('should return 200 and user by id ', (done) => {
    agent
      .get(`/user/${userOneId}`)
      .expect(200)
      .end((err, res) => {
        expect(res.body).to.have.property('_id');
        expect(res.body).to.have.property('email');
        expect(res.body._id).equal(userOneId);
        expect(res.body.email).equal(require('../fixtures/userClient1.json').email);

        done(err);
      });
  });

  it('should return 200 and update user by id', (done) => {
    const updateEmail = `update.${require('../fixtures/userClient1.json').email}`;

    agent
      .put(`/user/${userOneId}`)
      .send({ email: updateEmail })
      .expect(200)
      .end((err, res) => {
        expect(res.body).to.have.property('nModified', 1);

        done(err);
      });
  });

  it('should return 200 and delete user by id', (done) => {
    agent
      .delete(`/user/${userOneId}`)
      .expect(200)
      .end((err, res) => {
        expect(res.body).to.have.property('success');

        done(err);
      });
  });

  after((done) => {
    mongoose.connection.collection('users').remove({}, done);
  });
});
